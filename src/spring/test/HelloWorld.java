package spring.test;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@SuppressWarnings("all")
public class HelloWorld {
	private String message;
	private int index;
	private static int count=0;

	public HelloWorld() {
		count++;
		this.index = count;
	}

	public HelloWorld(String message) {
		this();
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@PostConstruct
	public void init() {
		System.out.println("Inside init of index " + index);
	}

	@PreDestroy
	public void destroy() {
		System.out.println("Inside destroy of index " + index);
	}

	public void getMessage() {
		System.out.println("Your Message : " + message + " count: " + count);
	}
}
