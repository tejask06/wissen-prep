package spring.test;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SuppressWarnings("resource")
public class MainApp {
	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		context.start();
		HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
		obj.getMessage();
		context.registerShutdownHook();
		context.stop();
		System.out.println("Post registration");

		AbstractApplicationContext ctx = new AnnotationConfigApplicationContext(HelloWorldConfig.class);
		ctx.start();

		HelloWorld helloWorld = ctx.getBean(HelloWorld.class);
		helloWorld.setMessage("Hello World!");
		helloWorld.getMessage();

		HelloWorld helloWorld2 = ctx.getBean(HelloWorld.class);
		helloWorld2.setMessage("Hello World 2!");
		helloWorld2.getMessage();
		ctx.stop();
		ctx.registerShutdownHook();
	}
}
