package spring.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class HelloWorldConfig {

	@Bean(initMethod = "init", destroyMethod = "destroy")
	@Scope("singleton")
	public HelloWorld hello() {
		System.out.println("printing from java config");
		return new HelloWorld();
	}

	@Bean
	public CStartEventHandler startHandler() {
		return new CStartEventHandler();
	}

	@Bean
	public CStopEventHandler stopHandler() {
		return new CStopEventHandler();
	}

	@Bean
	public EventHandler handler() {
		return new EventHandler();
	}
}
