package tejas.prep;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class GenericObjectPool<T extends PoolMember> {

	private BlockingQueue<T> pool = null;

	public GenericObjectPool(Class<T> clazz, int n) throws InstantiationException, IllegalAccessException {
		pool = new ArrayBlockingQueue<>(n);
		//for (int i = 0; i < n; i++) {
		do {
			T newObj = clazz.newInstance();
			if (newObj.initialize()) {
				// TODO: What if initialization fails??
				pool.offer(newObj);
			}
		} while (pool.size() != n);
	}

	public void shutdown() {
		for (T t : pool) {
			t.destroy();
		}
	}

	public T get() throws InterruptedException {
		System.out.println("Getting object");
		return pool.remove(); // Will remove from the pool so no need to track in use objects
	}

	public T blockingGet() throws InterruptedException {
		return pool.take();
	}

	public void release(T object) { // Since objects are being removed when getting, no need to worry about consumer never releasing an object.
		System.out.println("Releasing object");
		pool.add(object);
	}

	public void blockingRelease(T object) { // Since objects are being removed when getting, no need to worry about consumer never releasing an object.
		System.out.println("Releasing object");
		pool.offer(object);
	}

	public static void main(String[] args) {
		try {
			int size = 10;
			GenericObjectPool<ConcreteMember> testPool = new GenericObjectPool<ConcreteMember>(ConcreteMember.class, size);
			Runnable[] getters = new Getter[size];
			for (Runnable t : getters) {
				t = new Getter<>(testPool);
				t.run();
			}
			//			List<ConcreteMember> objs = new ArrayList<>(size);
			//			for (int i = 0; i < size; i++) {
			//				ConcreteMember e = testPool.get();
			//				objs.add(e);
			//			}
			//			testPool.blockingGet();
			//
			//			for (ConcreteMember blah : objs) {
			//				testPool.release(blah);
			//			}
			//			ConcreteMember c = new ConcreteMember();
			//			testPool.release(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Getter<T extends PoolMember> implements Runnable {
	private GenericObjectPool<T> pool;

	public Getter(GenericObjectPool<T> pool) {
		super();
		this.pool = pool;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(50);
			T object = pool.get();
			new Putter<T>(pool, object).run();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class Putter<T extends PoolMember> implements Runnable {

	private GenericObjectPool<T> pool;
	T member;

	public Putter(GenericObjectPool<T> pool, T member) {
		super();
		this.pool = pool;
		this.member = member;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pool.release(member);
	}

}

interface PoolMember {
	public boolean initialize();

	public void destroy();
}

class ConcreteMember implements PoolMember {

	private static int count = 0;
	private int index;

	@Override
	public boolean initialize() {
		index = count;
		System.out.println("Count increased to " + ++count);
		return true;
	}

	public Integer getIndex() {
		return index;
	}

	@Override
	public void destroy() {
		System.out.println("Count decreased to " + --count);
	}

}