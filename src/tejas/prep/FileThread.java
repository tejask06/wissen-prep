package tejas.prep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FileThread {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		//		ExecutorService threadPool = Executors.newFixedThreadPool(10);
		ExecutorService threadPool = Executors.newCachedThreadPool();
		Map<String, Future<Integer>> fileToFuture = new HashMap<>();
		File folder = new File("/home/tejas/Desktop/test"); // Directory contains files named batchxx that contains a number on every line
		List<Future<Integer>> futures = new ArrayList<>();
		for (File file : folder.listFiles()) {
			if (file.getName().startsWith("batch")) {
				System.out.println("Submitting file " + file.getName());
				String absolutePath = file.getAbsolutePath();
				Future<Integer> future = threadPool.submit(new FileParser(absolutePath));
				fileToFuture.put(absolutePath, future);
			}
		}
		int finalSum = 0;
		for (String filePath : fileToFuture.keySet()) {
			Future<Integer> future = fileToFuture.get(filePath);
			try {
				Integer retVal = future.get();
				if (retVal != null)
					finalSum += retVal;
				else
					System.out.println("Failed for " + filePath);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Final sum is " + finalSum);
		System.out.println("Time taken is " + (System.currentTimeMillis() - start));
		threadPool.shutdown();
	}
}

class FileParser implements Callable<Integer> {

	String path;

	public FileParser(String path) {
		super();
		this.path = path;
	}

	@Override
	public Integer call() throws Exception {
		Integer sum = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			for (String line; (line = br.readLine()) != null;) {
				sum += Integer.parseInt(line);
			}
		} catch (Exception e) {
			sum = null;
			e.printStackTrace();
		}
		System.out.println("Sum for " + path + " is " + sum);
		return sum;
	}
}