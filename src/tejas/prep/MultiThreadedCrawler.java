package tejas.prep;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MultiThreadedCrawler implements Runnable {
	private String url;

	public MultiThreadedCrawler(String url) {
		super();
		this.url = url;
	}

	@Override
	public void run() {
		Document doc;
		try {
			System.out.println("Loading " + this.url);
			doc = Jsoup.connect(this.url).get();
			System.out.println(doc.title());
			Elements anchorTags = doc.select("a");
			for (Element anchor : anchorTags) {
				String nextUrl = anchor.attr("href");
				System.out.println("Enqueueing " + nextUrl);
				CrawlerPool.submitCrawler(new MultiThreadedCrawler(nextUrl));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		CrawlerPool.submitCrawler(new MultiThreadedCrawler("http://www.infibeam.com"));
	}
}

class CrawlerPool {
	private static ExecutorService ThreadPool = Executors.newFixedThreadPool(10);
	
	public static void submitCrawler(MultiThreadedCrawler task) {
		ThreadPool.submit(task);
	}
}